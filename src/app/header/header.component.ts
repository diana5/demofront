import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario/usuario';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public user= <Usuario>JSON.parse(localStorage.getItem('yo'));
  public usuario=  this.user.nombres;
  constructor() {}

  ngOnInit() {
  }

}
