import { Component, OnInit } from '@angular/core';
import { NotasService } from '../notas/notas.service';
import { Usuario } from '../usuario/usuario';
import { UsuarioService } from '../usuario/usuario.service';
import { Notas } from '../notas/notas';

@Component({
  selector: 'app-notas-usuario',
  templateUrl: './notas-usuario.component.html',
  styleUrls: ['./notas-usuario.component.css']
})
export class NotasUsuarioComponent implements OnInit {
  notasById: any;
  clavesPorId: Object;
  public usuario=<Usuario>JSON.parse(localStorage.getItem('yo'));
  
  constructor(private usuarioService: UsuarioService, private notaService:NotasService) { }


  public getNotasById(){
    this.usuarioService.getNotasById(this.usuario.id)
    .subscribe(
      result=> {
        this.notasById=result;
        console.log(this.notasById);

      }
    )
  }

  public eliminarNotas(id : number){
    this.notaService.deleteNotas(id)
    .subscribe(
      result =>{
        console.log(result);
       this.getNotasById();

        
      }
    )
  
  }



  ngOnInit() {
    this.getNotasById();
  }

}
