import { Component, OnInit } from '@angular/core';
import { Usuario } from './usuario';
import { Router } from '@angular/router';
import { UsuarioService } from './usuario.service';
import { Imagen } from '../carga/imagen';
import { NuevoUsuarioComponent } from '../nuevo-usuario/nuevo-usuario.component';
import { NuevousuarioService } from '../nuevo-usuario/nuevousuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit{
public usuario:Usuario;
public show=false
public img:Imagen;
public ruta:string;
public alternativa:string ="src/assets/uploads/vistaprevia.jpg";
  datosusuario: Usuario;
constructor(private router: Router,
            private usuarioService: UsuarioService,
            private usuarioServ: NuevousuarioService
            ) {
  this.usuario=<Usuario>JSON.parse(localStorage.getItem('yo'));


 }

 public mostrar()
{
  this.show=true
}

public getFotoPerfil(){
  console.log(this.usuario.id)
  this.usuarioService.getPhoto(JSON.stringify(this.usuario.id))
  .subscribe(
    result=>{
      console.log(result)
      this.img=<Imagen>result;

      this.ruta=this.img.ruta;
      if (this.img.ruta==null)
      console.log(this.img);
    }
  )
}

public getDatosUsuario(){
  this.usuarioServ.getDatosUsuario(this.usuario.id)
  .subscribe(
    result=>{
      console.log(result)
      this.datosusuario=<Usuario>result;
    }
  )
  
}

 public salir() {
  localStorage.clear();
  this.router.navigate(['/']);
}

public ngOnInit(){
  this.getFotoPerfil();
  this.getDatosUsuario();
}
}
