import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Oauth } from '../oauth/oauth';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tokenKey } from '@angular/core/src/view';
import {Usuario} from '../usuario/usuario';
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private oauth = <Oauth>JSON.parse(localStorage.getItem('oauth'));
  private usuariosUrl = environment.apiUrl + '/V1/usuario';
  private imagenUrl=environment.apiUrl + '/V1/img';
  constructor(private http: HttpClient) { }
  public login(usuario: String, pass: String) {
    const headers =
      new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Authorization', 'Basic ' + btoa(environment.apiUser + ':' + environment.apiPass));

    return this.http.post(environment.apiUrl + '/oauth/token',
      encodeURI('grant_type=password&username=' + usuario + '&password=' + pass), { headers: headers });
  }
  public perfil(token: string) {
    const headers =
      new HttpHeaders().set('Authorization', 'Bearer ' + token);
    return this.http.get(this.usuariosUrl + '/yo', { headers: headers });
  }

  public getUsuarios(q: string, page: number, size: number) {
    const headers = new HttpHeaders().set(
      'Authorization',
      'Bearer' + this.oauth.access_token);
    return this.http.get(this.usuariosUrl + '?page=' + page + '&size=' + size + '&q=' + q, { headers: headers });

  }



 
  public getNotasById(id:number){
    const headers =new HttpHeaders
    ({'Content-Type': 'application/json'})
    .set('Authorization','Bearer'+ this.oauth.access_token);
    return this.http.get(this.usuariosUrl+'/notasporid/'+id,{headers:headers})
  }

  public deleteNotasById(){
    const headers= new HttpHeaders
    ({'Content-Type':'application/json'})
    .set('Authorization','Bearer'+this.oauth.access_token);
    return this.http.get(this.usuariosUrl+'/clavesporid', {headers:headers})
  }

  
  public obtenerClavesById(id:number){
    const headers= new HttpHeaders
    ({'Content-Type':'application/json'})
    .set('Authorization','Bearer'+ this.oauth.access_token);
    return this.http.get(this.usuariosUrl+'/clavesporid/'+ id, {headers:headers})
  }

  public getPhoto(id:string){
    const headers=new HttpHeaders({'Content-Type':'application/json'})
    .set('Authorization','Bearer'+ this.oauth.access_token);
    return this.http.get(this.imagenUrl +'/'+ id, {headers:headers})
  
  }


}
