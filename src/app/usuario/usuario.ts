import { Personarol } from "../personarol/personarol";

export class Usuario {
    constructor(public id: number, public login: string, public nombres: string, public apellidos: string, public telefono: number, public email: string, public rol: Personarol, public clave: string, public foto:File) { }
}
