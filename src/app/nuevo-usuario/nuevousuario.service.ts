import { Injectable } from '@angular/core';
import { Usuario } from '../usuario/usuario';
import { HttpHeaderResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { Oauth } from '../oauth/oauth';
import { environment } from '../../environments/environment';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class NuevousuarioService {

  private oauth=<Oauth>JSON.parse(localStorage.getItem('oauth'))
  private mantenedorUrl= environment.apiUrl+'/V1/usuario';
  constructor( private http: HttpClient){

   }

  public addUser(usuario:Usuario){
      const headers= new HttpHeaders({'Content-Type':'application/json'})
      console.log(usuario)
      const body=usuario;
     return this.http.post(this.mantenedorUrl,body,{headers:headers})
      .subscribe(
        result=>{
          console.log(result);
          swal("usuario creado satisfactoriamente",'success')
        }
      )
  }



  //se  usa en el componente carga
  public editarUsuario(usuario:Usuario){
    const headers= new HttpHeaders({'Content-Type': 'application/json'})
    .set('Authorization','Bearer'+this.oauth.access_token)
    console.log(usuario)
    console.log(usuario.id);
     return this.http.put(this.mantenedorUrl + '/'+ usuario.id, usuario ,{headers:headers})
     .subscribe(
      result=> {
        console.log(result);
        window.location.reload();
      }
    )
  }

  public getDatosUsuario(id:number){
    const headers= new HttpHeaders({'Content-Type': 'application/json'})
    .set('Authorization','Bearer'+this.oauth.access_token);
    return this.http.get(this.mantenedorUrl + '/' + id, {headers:headers})


  }
}
