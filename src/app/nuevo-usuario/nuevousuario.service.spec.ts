import { TestBed } from '@angular/core/testing';

import { NuevousuarioService } from './nuevousuario.service';

describe('NuevousuarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NuevousuarioService = TestBed.get(NuevousuarioService);
    expect(service).toBeTruthy();
  });
});
