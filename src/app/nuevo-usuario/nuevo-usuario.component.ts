import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario/usuario';
import { UsuarioService } from '../usuario/usuario.service';
import swal from 'sweetalert2';
import { Personarol } from '../personarol/personarol';
import { NuevousuarioService } from './nuevousuario.service';
import { Oauth } from '../oauth/oauth';

@Component({
  selector: 'app-nuevo-usuario',
  templateUrl: './nuevo-usuario.component.html',
  styleUrls: ['./nuevo-usuario.component.css']
})
export class NuevoUsuarioComponent implements OnInit {

private usuarioActual=<Usuario>JSON.parse(localStorage.getItem('yo'));
public rol = new Personarol(1,"Administrador")
public usuario= new  Usuario(null,'','','',null,'', this.rol ,'',null);
  apellidos: string; 
  email: string;
  telefono: number;
  nombres: string;
  login: string;
  clave: string;
  hash: any;
  nombre: any;
  

  constructor(public usuarioService: NuevousuarioService,
   private usuarioServ: UsuarioService,
  ) {}

  ngOnInit() {
  }

 
  public agregarUsuario(){
    this.usuario.apellidos=this.apellidos;
    this.usuario.email=this.email;
    this.usuario.nombres=this.nombres;
    this.usuario.rol.id=1;
    this.usuario.rol.descripcion="Administrador";
    this.usuario.telefono=this.telefono;
//aqui se tiene que usar ssl, si no, se tiene que encriptar
    this.usuario.clave=this.clave;
    this.usuario.login=this.login;
    this.usuarioService.addUser(this.usuario)
    
  }

}
