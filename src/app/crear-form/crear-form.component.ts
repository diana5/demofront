import { Component, OnInit } from '@angular/core';
import { Clave } from '../clave/clave';
import { ActivatedRoute, Router } from '@angular/router';
import { MantenedorService } from '../mantenedor/mantenedor.service';
import swal from 'sweetalert2'
import { Usuario } from '../usuario/usuario';
import { Personarol } from '../personarol/personarol';

@Component({
  selector: 'app-crear-form',
  templateUrl: './crear-form.component.html',
  styleUrls: ['./crear-form.component.css']
})
export class CrearFormComponent implements OnInit {
  
  public name='';
  public apellido='';
  public email='';
  public fecha :Date=null;
  public descripcion='';
  public pass='';
rol: Personarol=new Personarol(1,"Administrador")
 user: Usuario= new Usuario (null,'','','',null,'',this.rol,'',null);
  clave: Clave= new Clave(null,'','','','',null,'','',this.user);
  constructor(private mantenedorService: MantenedorService,
    private router: Router,
  private activatedRoute: ActivatedRoute) { }
private usuarioObj= <Usuario>JSON.parse(localStorage.getItem('yo'));  
public create() {
  console.log(this.usuarioObj)
    this.clave.usuario.id=this.usuarioObj.id;
    this.clave.usuario.login=this.usuarioObj.login;
    this.clave.name=this.name;
    this.clave.email=this.email;
    this.clave.fecha=this.fecha;
    this.clave.apellido=this.apellido;
    this.clave.notas=this.descripcion;
    this.clave.clave=this.pass;
    console.log(this.clave)
    this.mantenedorService.addClaves(this.clave)
      .subscribe(clave => {
        console.log(clave) 
        swal('Se Agrego Su Registro correctamente', 'success');
      }
      );
  }


  ngOnInit() {
  }

  

}
