import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Oauth } from '../oauth/oauth';
import { UsuarioService } from '../usuario/usuario.service';
import { faSignOutAlt, faStore, faSignInAlt, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public usuario: string;
  public pass: string;
  public cargando = false;
  //uso los iconos que importe de font awesome arriba
  public faSignInAlt = faSignInAlt;
  public faEye = faEye;
  public faEyeSlash = faEyeSlash;
  typePass: string='password';
  constructor(
    private usuarioService: UsuarioService,
    private router: Router,
    private http: HttpClient,
  ) { }

  public login() {
    this.usuarioService.login(this.usuario, this.pass).subscribe(
      result => {
        const oauth: Oauth = <Oauth>result;
        this.usuarioService.perfil(oauth.access_token).subscribe(
          result2 => {
            this.cargando = false;
            localStorage.setItem('oauth', JSON.stringify(result));
            localStorage.setItem('yo', JSON.stringify(result2));
            this.router.navigate(['/usuario']);
          },

          error2 => {
            this.cargando = false;
            console.error(<any>error2);
          }
        );
      },
      error => {
        this.cargando = false;
        console.error(<any>error);
      }
    );
  }

  public cambiaTypePass() {
    if (this.typePass === 'password') {
      this.typePass = 'text';
    } else {
      this.typePass = 'password';
    }

  }
}
