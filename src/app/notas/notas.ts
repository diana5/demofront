import { Usuario } from "../usuario/usuario";

export class Notas {
    constructor(public id: number, public titulo: string, public descripcion: string, public usuario: Usuario){}
}
