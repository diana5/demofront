import { Component, OnInit } from '@angular/core';
import { NotasService } from './notas.service';
import { Notas } from './notas';
import { UsuarioService } from '../usuario/usuario.service';
import { Usuario } from '../usuario/usuario';
import { Personarol } from '../personarol/personarol';
import swal from 'sweetalert2';

@Component({
  selector: 'app-notas',
  templateUrl: './notas.component.html',
  styleUrls: ['./notas.component.css']
})
export class NotasComponent implements OnInit {

  public rol=new Personarol(1,'Administrador')
  public usuario= new Usuario(null,'','','',null,'',this.rol,'',null)
  public nota= new Notas(null,'','',this.usuario)
  public arreglousuarios:any;
  idUsuarioActual: any;
  //aqui me devoldia un json, pero tuve que decirle del objeto para poderlo leer
  public nombreusuario= <Usuario>JSON.parse(localStorage.getItem('yo'));
  public titulo: string;
  public descripcion: string;
  public id;
  todasLasNotas: any;
  public show=false;
  todasNotas: any;
 public notas;
  
  
  
  constructor(private notasService: NotasService,
     private usuarioService:UsuarioService,
     private notaService:NotasService) { 
  }

public agregarNota(notas:Notas){
  this.nota.descripcion=this.descripcion;
  this.nota.titulo=this.titulo;
  console.log(this.nombreusuario.id)
  this.nota.usuario.id=this.nombreusuario.id;
  this.nota.usuario.login=this.nombreusuario.login;
  this.nota.usuario.apellidos=this.nombreusuario.apellidos;
  this.nota.usuario.email=this.nombreusuario.email;
  this.nota.id=this.id;
  this.notasService.addNotas(this.nota)    .subscribe(
    result => {
      console.log(result);
      swal("Su nota se ha creado correctamente");
      //para que recarge las notas otra vez cuando agregue una
      this.obtenerNotas();
    }
  );
}

public obtenerNotas(){
  this.notasService.getNotas()
  .subscribe(
    resultado=>{
      console.log(resultado);
      // un objeto de tipo: [{valior:1, valor: 2}]
      this.todasLasNotas = resultado;
      //convertir a arreglo para poder usar sort
    //  const arreglo = Object.keys(this.todasLasNotas).map
    //  (key => ({type: key, value: this.todasLasNotas[key] }));
      //muestra alreves todo
    // this.todasNotas = arreglo.reverse();
     //this.notas= JSON.stringify(arreglo.values);
     //console.log(arreglo.values.toString);
     
    }
  );
}

public eliminarNotas(id: number) {
  this.notaService.deleteNotas(id)
  .subscribe(
    result => {
      console.log(result);
      this.obtenerNotas();
    }
  );

}
  ngOnInit() {
    this.obtenerNotas();
  }



}
