import { Injectable } from '@angular/core';
import { Notas } from './notas';
import {  HttpHeaderResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { Oauth } from '../oauth/oauth';
import { environment } from '../../environments/environment';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class NotasService {

  private oauth=<Oauth>JSON.parse(localStorage.getItem('oauth'))
  private mantenedorUrl=environment.apiUrl+'/V1/notas';
  constructor(private http: HttpClient) { }

  public addNotas(nota: Notas){
   const headers= new HttpHeaders
   ({'Content-Type':'application/json'})
   .set('Authorization','Bearer'+this.oauth.access_token);
   console.log(nota);
   const body=nota;
   return this.http.post(this.mantenedorUrl,body, {headers:headers})

  }

  public getNotas(){
    const headers= new HttpHeaders
    ({'Content-Type':'application/json'})
    .set('Authorization','Bearer'+this.oauth.access_token);
    return this.http.get(this.mantenedorUrl,{headers:headers})

  }

  public deleteNotas(id: number){
    console.log(id);
    const headers= new HttpHeaders
    ({'Content-Type': 'application/json'})
    .set('Authorization','Bearer'+this.oauth.access_token);
    return this.http.delete(this.mantenedorUrl+'/'+id,{headers:headers})

  }

}
