import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonarolComponent } from './personarol.component';

describe('PersonarolComponent', () => {
  let component: PersonarolComponent;
  let fixture: ComponentFixture<PersonarolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonarolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonarolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
