import { TestBed } from '@angular/core/testing';

import { PersonarolService } from './personarol.service';

describe('PersonarolService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PersonarolService = TestBed.get(PersonarolService);
    expect(service).toBeTruthy();
  });
});
