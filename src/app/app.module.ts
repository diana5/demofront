import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { PersonarolComponent } from './personarol/personarol.component';
import { OauthComponent } from './oauth/oauth.component';
import { environment} from './environment/environment';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
 import { HttpModule } from '@angular/http';
import { MantenedorComponent } from './mantenedor/mantenedor.component';
import { ClaveComponent } from './clave/clave.component';
import { ResultComponent } from './result/result.component';
import { CrearFormComponent } from './crear-form/crear-form.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NuevoUsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';
import { DatePipe } from '@angular/common';
import {MatSidenavModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import { NotasComponent } from './notas/notas.component';
import { NotasUsuarioComponent } from './notas-usuario/notas-usuario.component';
import { ClavesUsuarioidComponent } from './claves-usuarioid/claves-usuarioid.component';
import { HeaderComponent } from './header/header.component';
import { CargaComponent } from './carga/carga.component';
import { ReversePipe } from './reverse.pipe';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  {path: 'personarol', component: PersonarolComponent},
  {path: 'usuario', component: UsuarioComponent},
  {path: 'form', component: CrearFormComponent},
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '', redirectTo: '/usuario', pathMatch: 'full' },
  { path: 'personarol', component: PersonarolComponent },
  {path: 'form/:id', component: CrearFormComponent},
  {path: 'registrar', component: NuevoUsuarioComponent},
  {path: 'mis-notas', component: NotasUsuarioComponent},
  {path: 'mis-claves', component: ClavesUsuarioidComponent},
  {path: 'cargar-img', component: CargaComponent }


];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsuarioComponent,
    PersonarolComponent,
    OauthComponent,
    MantenedorComponent,
    ClaveComponent,
    ResultComponent,
    CrearFormComponent,
    NuevoUsuarioComponent,
    NotasComponent,
    NotasUsuarioComponent,
    ClavesUsuarioidComponent,
    HeaderComponent,
    CargaComponent,
    ReversePipe
      ],
  imports: [
    BrowserModule,
    FormsModule,
    ShowHidePasswordModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FontAwesomeModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule


  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
