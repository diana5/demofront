import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClavesUsuarioidComponent } from './claves-usuarioid.component';

describe('ClavesUsuarioidComponent', () => {
  let component: ClavesUsuarioidComponent;
  let fixture: ComponentFixture<ClavesUsuarioidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClavesUsuarioidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClavesUsuarioidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
