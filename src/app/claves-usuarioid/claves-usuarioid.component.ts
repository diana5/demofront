import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuario/usuario.service';
import { Clave } from '../clave/clave';
import { MantenedorService } from '../mantenedor/mantenedor.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Personarol } from '../personarol/personarol';
import { Usuario } from '../usuario/usuario';

@Component({
  selector: 'app-claves-usuarioid',
  templateUrl: './claves-usuarioid.component.html',
  styleUrls: ['./claves-usuarioid.component.css']
})
export class ClavesUsuarioidComponent implements OnInit {
  clavesPorId: Object;
  actualizar: number;
  rol: Personarol=new Personarol(1,"Administrador")
  user: Usuario= new Usuario (null,'','','',null,'',this.rol,'',null);
  clave: Clave= new Clave(null,'','','','',null,'','',this.user);
  private usuarioObj= <Usuario>JSON.parse(localStorage.getItem('yo'));  

  public tamano=100;
  public pagina=100;
  public q="";
  public datos:any;
  public nombre;
  public apellido;
  public email;
  public fecha:Date;
  public pass;
  public notas;
  item: any;
  numreload=0;
  public id;

  constructor(public usuarioService: UsuarioService,
    public mantenedorService: MantenedorService,
    public router:Router,
    public activeroute:ActivatedRoute) { 

      this.activeroute.params.subscribe(
        result=>{
        this.getClaveById(); 
        console.log(activeroute)
      }
      )
    }
    
   
    public deleteKeys(id:number){
      console.log(id);
      this.mantenedorService.eliminarClaves(id)
      .subscribe(
        result_=>{
          console.log("hecho");
          console.log(result_)
          window.location.reload();
    
    
        }
      );
    }
    //verifica la id y muestra el editar
    public verificarId(id: number){
      this.actualizar=id;
      return this.actualizar;
    
    }
    
    public updateKeys(id:number){
      this.item=this.datos.find(x=>x.id==this.actualizar);
      console.log(this.item)
      console.log( this.nombre,this.apellido,this.datos)
      if (this.nombre==null){
        this.nombre=this.item.name;
      }
      if (this.apellido==null){
        this.apellido=this.item.apellido;
      }
      if (this.notas==null){
        this.notas=this.item.notas;
      }
      if (this.fecha==null){
        this.fecha=this.item.fecha;
      }
      if (this.email==null){
        this.email=this.item.email;
      }
      if (this.pass==null){
        this.pass=this.item.clave;
      }
      console.log(this.pass)
      this.clave.id = this.actualizar;
      this.clave.name = this.nombre;
      this.clave.apellido = this.apellido;
      this.clave.notas = this.notas;
      this.clave.fecha = this.fecha;
      this.clave.email = this.email;
      this.clave.clave = this.pass;
      console.log(this.clave)
      this.mantenedorService.editarClaves(this.clave);
    
    }
    
  public getClaveById(){
   
    this.usuarioService.obtenerClavesById(this.usuarioObj.id)
    .subscribe(
      result=>{
        console.log(result);
        this.datos=result; 
        
        this.router.navigate(['/mis-claves'])


      }
    )
  }

  ngOnInit() {

    
    this.getClaveById();
  }

}
