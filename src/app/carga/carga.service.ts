import { Injectable } from '@angular/core';
import { HttpClient,HttpResponse, HttpHeaderResponse, HttpHeaders, HttpEvent, HttpRequest} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Oauth } from '../oauth/oauth';
import { Observable } from 'rxjs';
import { Usuario } from '../usuario/usuario';

@Injectable({
  providedIn: 'root'
})
export class CargaService {
  
  private oauth=<Oauth>JSON.parse(localStorage.getItem('oauth'));
  private usuarioUrl= environment.apiUrl+'/V1/usuario';
  
  public usuario=<Usuario>JSON.parse(localStorage.getItem('yo'));

  constructor(public http:HttpClient) { }
//envia la info  la base de datos, se puede hacer 
/*
asi
o como formdata
onUpload() {
  // this.http is the injected HttpClient
  const uploadData = new FormData();
  uploadData.append('myFile', this.selectedFile, this.selectedFile.name);
  this.http.post('my-backend.com/file-upload', uploadData)
    .subscribe(...);
}
onUpload() {
  // this.http is the injected HttpClient
  this.http.post('my-backend.com/file-upload', this.selectedFile)
    .subscribe(...);
}

/** */

//cargar imamgen al servidor
public postFileImagen(imagenParaSubir: File){

  console.log(imagenParaSubir);
  const formData = new FormData(); 
  formData.append('imagenPropia', imagenParaSubir, imagenParaSubir.name); 
  const body=JSON.stringify(formData);
  const headers=new HttpHeaders({'Contet-Type':'application/json'})
  .set('Authorization','Bearer'+ this.oauth.access_token,
);

const req = new HttpRequest('POST', this.usuarioUrl, formData);
return this.http.request(req);
}

//eliminar imagen d el servidor
public eliminarUsuario(id:number){
  const headers= new HttpHeaders({'Content-Type':'aplication/json'})
  .set('Authorization','Bearer' + this.oauth.access_token);
  const user=JSON.stringify(id);
  return this.http.delete(this.usuarioUrl + '/eliminar' + '/' + user , {headers:headers})
  .subscribe(
    result=>console.log(result)
  )

}

}
