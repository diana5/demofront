import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CargaItemFile } from './carga-item-file';
import { CargaService } from './carga.service';
import { Observable } from 'rxjs';
import { Usuario } from '../usuario/usuario';
import { environment } from '../../environments/environment';
import { HttpHeaders, HttpRequest, HttpClient } from '@angular/common/http';
import { Oauth } from '../oauth/oauth';
import { MAT_CHIPS_DEFAULT_OPTIONS } from '@angular/material';
import { UsuarioService } from '../usuario/usuario.service';
import { Personarol } from '../personarol/personarol';
import { NuevousuarioService } from '../nuevo-usuario/nuevousuario.service';
import { Imagen } from './imagen';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';



@Component({
  selector: 'app-carga',
  templateUrl: './carga.component.html',
  styleUrls: ['./carga.component.css']
})
export class CargaComponent implements OnInit {


  public oauth = <Oauth>JSON.parse(localStorage.getItem('oauth'));
  public usuario = <Usuario>JSON.parse(localStorage.getItem('yo'));
  public respuestaImagenEnviada;
  public resultadoCarga;
  public actualizar: number;
  public form;
  private usuarioUrl = environment.apiUrl + '/V1/usuario';
  private usuarioActual = <Usuario>JSON.parse(localStorage.getItem('yo'));
  public rol = new Personarol(1, "Administrador")
  public usuarioEdit = new Usuario(null, '', '', '', null, '', this.rol, '', null);
  nombre: string;
  apellidos: string;
  email: string;
  telefono: number;
  id: number;
  file: string;
  nombrefile: string;
  public ruta: any;
  public fotodeperfil: Imagen;
  fotourl: SafeResourceUrl;
  mostrar: boolean = false;
  cargaImagen: boolean;
  public alternativa:string ="src/assets/uploads/vistaprevia.jpg";
  datosusuario: Usuario;



  constructor(private cargaService: CargaService,
    private http: HttpClient,
    private usuarioServ: NuevousuarioService,
    private usuarioService: UsuarioService,
    private sanitizer: DomSanitizer,
    private router:Router

  ) { }
  public carga: CargaItemFile[] = []


  

  public enviar() {
    this.form = document.forms.namedItem("fileinfo");
    this.form.addEventListener('submit', function (ev) {


      this.oOutput = document.getElementById("output"),
        this.oData = new FormData(document.forms.namedItem("fileinfo"));

      console.log(this.form)
      this.oData.append("archivo", this.form);

      this.oReq = new XMLHttpRequest();
      this.oReq.open("POST", this.usuarioUrl, true);
      this.oReq.onload = function (oEvent) {
        if (this.oReq.status == 200) {
          this.oOutput.innerHTML = "Uploaded!";
        } else {
          this.oOutput.innerHTML = "Error " + this.oReq.status + " occurred uploading your file.<br \/>";
        }
      };

      this.oReq.send(this.oData);
      ev.preventDefault();
    }, false);


  }

//retorna los datos del usuario actualizados
  public getUserData(){
    this.usuarioServ.getDatosUsuario(this.usuario.id)  
    .subscribe(
      result=>{
      console.log(result);
      this.datosusuario=<Usuario>result;
      }
    )

    

  }

  onFileChange(event) {
    let file = event.target.files[0];
    this.nombrefile = file.name;
    let formData = new FormData();
    console.log(formData)
    console.log(this.ruta)

    console.log(this.nombrefile)


    formData.append("archivo", file);
    //aqui tengo que ponerle en el append el mismo nombre que el @requestparam 
    formData.append("login", JSON.stringify(this.usuario.id));
    console.log(this.usuario.id)
    //MediaType.APPLICATION_XML
    formData.append('Content-Type', 'multipart/form-data');

    formData.append('Authorization', 'Bearer' + this.oauth.access_token)
    console.log(formData)
    return this.http.post(this.usuarioUrl + '/img', formData)
      .subscribe(
        result => {
          console.log(result)
          window.location.reload();

        }
      )


  }

  //verifica la id y muestra el editar
  public verificarId(id: number) {
    this.actualizar = id;
    return this.actualizar;

  }


  //uso el servicio de USUARIO SERVICE para editar el usuario
  public updateUsuario(id: number) {
    if (this.nombre == null)
      this.nombre = this.usuarioActual.nombres;
    if (this.apellidos == null)
      this.apellidos = this.usuarioActual.apellidos
    if (this.email == null)
      this.email = this.usuarioActual.email;
    if (this.telefono == null)
      this.telefono = this.usuarioActual.telefono;

    this.usuarioEdit.nombres = this.nombre;
    this.usuarioEdit.apellidos = this.apellidos;
    this.usuarioEdit.email = this.email;
    this.usuarioEdit.telefono = this.telefono;
    this.usuarioEdit.clave = this.usuarioActual.clave;
    this.usuarioEdit.rol = this.usuarioActual.rol;
    this.usuarioEdit.login = this.usuarioActual.login;
    this.usuarioEdit.foto = this.usuarioActual.foto;
    this.usuarioEdit.id = id
    console.log(this.usuarioEdit)
    this.usuarioServ.editarUsuario(this.usuarioEdit);

    window.location.reload();
    this.getUserData();

  }

  //llamo al metodo usuario, getPhoto del servicio usuario

  public getFoto() {
    console.log(this.ruta)
    this.usuarioService.getPhoto(JSON.stringify(this.usuarioActual.id))
      .subscribe(
        result => {
          this.fotodeperfil = <Imagen>(result);
          this.fotourl = this.sanitizer.bypassSecurityTrustResourceUrl(this.fotodeperfil.ruta);
          console.log(this.fotodeperfil.ruta)
        
          //AQUI FALTA CONVERTIRLO EN IMAGEN JSON
        })
  }

  public deleteFoto(){
    console.log(this.usuario.id)
    this.cargaService.eliminarUsuario(this.usuario.id)
    
  }

  public salir() {
    localStorage.clear();
    this.router.navigate(['/']);
  }

  ngOnInit() {
    this.getFoto()
    this.getUserData();
  }
}

