import { Usuario } from "../usuario/usuario";

export class Clave {
    constructor(public id:number,public name:string, public apellido:string, public email:string,
    public descripcion:string, public fecha:Date, public notas:string, public clave:string, public usuario:Usuario){}
}
