import { Component, OnInit } from '@angular/core';
import { MantenedorService } from './mantenedor.service';
import { Clave } from '../clave/clave';
import { Result } from '../result/result';
import { Usuario } from '../usuario/usuario';
import { SELECT_MULTIPLE_VALUE_ACCESSOR } from '@angular/forms/src/directives/select_multiple_control_value_accessor';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-mantenedor',
  templateUrl: './mantenedor.component.html',
  styleUrls: ['./mantenedor.component.css']
})
export class MantenedorComponent implements OnInit{
  actualizar: number;
  user:Usuario= new Usuario(null,'','','',null,'',null,'',null)
  clave: Clave= new Clave(null,'','','','',null,'','',this.user);

  public tamano=100;
  public pagina=100;
  public q="";
  public datos:any;
  public nombre;
  public apellido;
  public email;
  public fecha:Date;
  public pass;
  public notas;
  item: any;

 
  constructor(public mantenedorService: MantenedorService,
  public router:Router) {}

public getKeys(){
  this.mantenedorService.getClaves()
  .subscribe(
  
    result_=>{
      console.log(result_);
//
      //los datos svan a ser a un objeto del resultado del subscribe
      this.datos=result_; 
//recuerda que es con doble ==
     const prueba=this.datos.filter(x => x.id == 22);
      console.log(prueba)
      console.log(this.datos)
    },
    error=>{
      console.error(<any>error)
      console.log("ocurrio un error");
    }

  ); 
}


public deleteKeys(id:number){
  console.log(id);
  this.mantenedorService.eliminarClaves(id)
  .subscribe(
    result_=>{
      console.log("hecho");
      console.log(result_)
      window.location.reload();


    }
  );
}
//verifica la id y muestra el editar
public verificarId(id: number){
  this.actualizar=id;
  return this.actualizar;

}

public updateKeys(id:number){
  this.item=this.datos.find(x=>x.id==this.actualizar);
  console.log(this.item)
  console.log( this.nombre,this.apellido,this.datos)
  if (this.nombre==null){
    this.nombre=this.item.name;
  }
  if (this.apellido==null){
    this.apellido=this.item.apellido;
  }
  if (this.notas==null){
    this.notas=this.item.notas;
  }
  if (this.fecha==null){
    this.fecha=this.item.fecha;
  }
  if (this.email==null){
    this.email=this.item.email;
  }
  if (this.pass==null){
    this.pass=this.item.clave;
  }
  console.log(this.pass)
  this.clave.id = this.actualizar;
  this.clave.name = this.nombre;
  this.clave.apellido = this.apellido;
  this.clave.notas = this.notas;
  this.clave.fecha = this.fecha;
  this.clave.email = this.email;
  this.clave.clave = this.pass;
  console.log(this.clave)
  this.mantenedorService.editarClaves(this.clave);

}

ngOnInit() {
 this.getKeys();
}
  
}
