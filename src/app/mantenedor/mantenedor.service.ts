import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { UsuarioService } from '../usuario/usuario.service';
import { Oauth } from '../oauth/oauth';
import { environment } from '../../environments/environment';
import { Clave } from '../clave/clave';

@Injectable({
  providedIn: 'root'
})
export class MantenedorService {

private oauth=<Oauth>JSON.parse(localStorage.getItem('oauth'));
private mantenedorUrl= environment.apiUrl+'/V1/clave';

  constructor(
    private http: HttpClient,
  ) { }


  getClave(id:string){
    const headers= new HttpHeaders({'Content-Type':'application/json'})
    .set('Authorization','Bearer'+this.oauth.access_token);
    return this.http.get(this.mantenedorUrl+'/'+id,{headers:headers});

  }
  public getClaves(){
    const headers= new HttpHeaders({'Content-Type':'application/json'})
    .set('Authorization','Bearer'+this.oauth.access_token);
  
  return this.http.get(this.mantenedorUrl, {headers:headers}); }
  
 public addClaves(clave: Clave){
   const headers= new HttpHeaders({'Content-Type':'application/json'})
   .set('Authorization','Bearer'+this.oauth.access_token);
   console.log(clave)
   const body=clave;
   return this.http.post(this.mantenedorUrl,body,{headers:headers})
 }

 public eliminarClaves(id: number){
   const headers=new HttpHeaders
   ({'Content-Type': 'application/json'})
   .set('Authorization','Bearer'+this.oauth.access_token);
   console.log()
   return this.http.delete(this.mantenedorUrl+'/'+id,{headers:headers})
 }

 public editarClaves(clave:Clave){
   const headers= new HttpHeaders
   ({'Content-Type' : 'application/json'})
   .set('Authorization', 'Bearer'+this.oauth.access_token);
   console.log(clave)
   const body = clave;
   return this.http.put(this.mantenedorUrl + '/' + clave.id , body, {headers:headers})
   .subscribe(
     () => {
       
       window.location.reload();
     }
   );
 }

}
